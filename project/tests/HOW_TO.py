##
# The Python unit testing framework, sometimes referred to as "PyUnit," is a Python language version of JUnit, by Kent
# Beck and Erich Gamma. JUnit is, in turn, a Java version of Kent's Smalltalk testing framework. Each is the de facto
# standard unit testing framework for its respective language.
##

import unittest
import pymodbus
import sys


##
# Basic example
#
# The unittest module provides a rich set of tools for constructing and running tests. This section demonstrates that a
# small subset of the tools suffice to meet the needs of most users.
#
# Here is a short script to test three string methods:
##
class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)


##
# Skipping tests and expected failures
# Unittest supports skipping individual test methods and even whole classes of tests. In addition, it supports marking a
#  test as an "expected failure," a test that is broken and will fail, but shouldn't be counted as a failure on a
# TestResult.
#
# Skipping a test is simply a matter of using the skip() decorator or one of its conditional variants.
#
# Basic skipping looks like this:
##
class MyTestCase(unittest.TestCase):

    @unittest.skip("demonstrating skipping")
    def test_nothing(self):
        self.fail("shouldn't happen")

    @unittest.skipIf(pymodbus.__version__ < (1, 3), "not supported in this library version")
    def test_format(self):
        # Tests that work for only a certain version of the library.
        pass

    @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
    def test_windows_support(self):
        # windows specific testing code
        pass


##
# Running from the file:
#
# The final block shows a simple way to run the tests. unittest.main() provides a command-line interface to the test
# script. When run from the command line, the script produces an output that looks like this:
# | >> unittest.main()
# | ...
# | ----------------------------------------------------------------------
# | Ran 3 tests in 0.000s
# |
# | OK
#
#
# Instead of unittest.main(), there are other ways to run the tests with a finer level of control, less terse output,
# and no requirement to be run from the command line. For example, the last two lines may be replaced with:
# | >> suite = unittest.TestLoader().loadTestsFromTestCase(TestStringMethods)
# | >> unittest.TextTestRunner(verbosity=2).run(suite)
# | test_isupper(__main__.TestStringMethods)...ok
# | test_split(__main__.TestStringMethods)...ok
# | test_upper(__main__.TestStringMethods)...ok
# |
# | ----------------------------------------------------------------------
# | Ran 3 tests in 0.001s
# |
# | OK
# >>
# >>
##
if __name__ == '__main__':
    # | >> unittest.main()
    # OR
    # | >> suite = unittest.TestLoader().loadTestsFromTestCase(TestStringMethods)
    # | >> unittest.TextTestRunner(verbosity=2).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2).run(suite)
