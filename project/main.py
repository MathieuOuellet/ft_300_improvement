import os
import time
import keyboard
import struct
import numpy as np
import serial
import winsound

from mechatronics_toolbox.python_libs.Robotiq_API.Communication.ModbusRTU import RTUCommunication
from mechatronics_toolbox.python_libs.Utils.RobotiqDataframeBase import RobotiqDataframeBase

def get_registre(registre):
    data = com.get_custom_register(registre, 2)
    while len(data) == 0:
        data = com.get_custom_register(registre, 2)

    t = struct.unpack('>h', bytes(data))[0]

    return t


def get_load(charge, vide=0):
    forces_x = []
    forces_y = []
    forces_z = []
    moments_x = []
    moments_y = []
    moments_z = []
    forces = []

    for i in range(1):
        # Acquisition des force Fx, Fy, Fz
        forces_x=(get_registre(180)/100-zeros[0])
        forces_y=(get_registre(181)/100-zeros[1])
        forces_z=(get_registre(182)/100-zeros[2])

        # Acquisition des moments Mx, My, Mz
        moments_x = get_registre(183)/1000-zeros[3]
        moments_y = get_registre(184)/1000-zeros[4]
        moments_z = get_registre(185)/1000-zeros[5]

    forces = [forces_x, forces_y, forces_z,
              moments_x, moments_y, moments_z]

    return forces

def zero():
    zeros_fx = []
    zeros_fy = []
    zeros_fz = []
    zeros_mx = []
    zeros_my = []
    zeros_mz = []
    zeros = []

    for i in range(10):
        zeros_fx.append(get_registre(180)/100)
        zeros_fy.append(get_registre(181)/100)
        zeros_fz.append(get_registre(182)/100)

        # Acquisition des moments Mx, My, Mz
        zeros_mx.append(get_registre(183)/1000)
        zeros_my.append(get_registre(184)/1000)
        zeros_mz.append(get_registre(185)/1000)

    zeros = [np.mean(np.array(zeros_fx)), np.mean(np.array(zeros_fy)), np.mean(np.array(zeros_fz)),
              np.mean(np.array(zeros_mx)), np.mean(np.array(zeros_my)), np.mean(np.array(zeros_mz))]
    winsound.Beep(freq, duration)
    return zeros

print(" Début programme")
time.sleep(0.5)

# Initialisation son
duration = 1000  # milliseconds
freq = 1000  # Hz

# Connexion au capteur
print("\n Connexion au capteur ....")
port_com = str(input("Inscrire le nom du port com (ex: COM4):"))
com = RTUCommunication()
com.set_mod_bus_id(9)
com.connect_to_device(port=port_com,baud_rate=19200)
time.sleep(0.5)
print("\n Capteur connecté!")
time.sleep(0.5)

# Mise à zéro force
input("\n Presser sur ENTER pour mettre le capteur à zéro")
zeros = zero()
time.sleep(0.5)
print("\n Zéro effectué")
forces = get_load(0, vide=0)
print(" X: %.2f Y: %.2f Z: %.2f  " % (forces[0], forces[1], forces[2]))

# Initialisation de l'engeristrement
print("\n Initialisation de l'engeristrement")
file_name = input("Nom du fichier d'enregistrement :")

desktop_path = os.path.join(os.environ["HOMEPATH"], "Desktop")

data = RobotiqDataframeBase(directory=desktop_path,
                               test_name=file_name,
                               column_names=["Fx", "Fy", "Fz", "Mx", "My", "Mz", "Zéro effectué", "Temps (s)"])

input("\n Appuyer sur ENTER pour débuter l'acquisition")
zero_effectue = 0

start_time = time.time()
timestamp=0

while True:
    stop_time = time.time()
    time_var = stop_time-start_time
    timestamp = timestamp + time_var
    start_time = time.time()
    new_row = get_load(0, vide=1)
    new_row.append(zero_effectue)
    new_row.append(timestamp)
    data.append_new_row(new_row)
    print("Fx:%.2f Fy:%.2f Fz:%.2f  Mx:%.2f My:%.2f Mz:%.2f" % (new_row[0], new_row[1], new_row[2], new_row[3], new_row[4], new_row[5]))
    data.save_data_to_csv()
    if keyboard.is_pressed('z'):
        time.sleep(0.5)
        print("Remise à zéro!")
        zero_effectue = zero_effectue + 1
        zeros = zero()

    if keyboard.is_pressed('q'):
        print("\nAcquisition terminé!")
        break
data.display_fancy_plots(plots_x_axis="Temps (s)")
