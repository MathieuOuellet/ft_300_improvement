# Banc de test accélération

## Table of Contents

[TOC]


## Description


## Structure du projet
Arborescence du projet:
```
> banc_test_acceleration/
├──> [doc]/
│    └──> (auto-generated doc)
├──> includes/
│    └──> (programs to install)
├──> project/
│    ├──> libs/
│    │    ├──> __init__.py
│    │    └──> (all dev libs)
│    ├──> tests/
│    │    ├──> __init__.py
│    │    └──> (unit tests)
│    ├──> __init__.py
│    └──> main.py
├──> requirements.txt
├──> setup.sh
└──> README.md
```

 - `./includes`: Dossier comprenant le code source des librairies à compiler sur le système ou toute information sur les installations nécessaires au fonctionnement du programme.
 - `./projet/libs`: Dossier contenant tout le code python n'étant pas le programme principal (ou "top level").
 - `./projet/tests`: Dossier contenant les tests unitaires des librairies.
 - `./projet/main.py`: Point d'entrée du programme.
 - `./requirements.txt`: Fichier contenant les dépendances python du projet ([plus d'infos](https://pip.pypa.io/en/stable/user_guide/#requirements-files)).


## Dévelopement

En vue du développement, configurer la plate-forme de développement conformément aux spécifications mentionnées, comme pour le déploiement. S'assurer de **toujours respecter les contraintes des tests unitaires en place** lors de la modification du code. Documenter et tester le nouveau code.

## Déploiement

Configurer la plate-forme de déploiement conformément aux spécifications mentionnées. Ceci inclus d'exécuter la séquence de configuration (fichier `setup`) si disponible et/ou de procéder à l'installation des programmes fournis dans le `./includes`.

Pour lancer le programme, exécuter le programme principal ("top level") `main.py`.

